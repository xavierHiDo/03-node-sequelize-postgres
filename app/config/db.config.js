module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "postgres-admin",
  DB: "03-node-sequelize-postgres",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};